# 2023_dev_android_mon_profil

Petite application Android permettant d'introduire l'utilisation de préférences partagées (SharedPreferences).
L'application permet de définir un profil utilisateur.
Contexte : Cours de développement Mobile  - R4.A.11 - Fiche 4 - 2023 - J.-F. Condotta