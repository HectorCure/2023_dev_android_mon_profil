package iutlens.android.monprofil

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity(), OnClickListener {
    private val fichierPreferences="prefsProfil"
    private lateinit var bt_editer: Button
    private lateinit var tv_pseudo: TextView
    private lateinit var tv_pays:TextView
    private lateinit var tv_age:TextView
    private lateinit var preferences:SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bt_editer=findViewById(R.id.id_bt_editer)
        tv_pseudo=findViewById(R.id.id_tv_pseudo)
        tv_pays=findViewById(R.id.id_tv_pays)
        tv_age=findViewById(R.id.id_tv_age)
        bt_editer.setOnClickListener(this)
        preferences = getSharedPreferences(fichierPreferences, Context.MODE_PRIVATE)
    }

    override fun onResume(){
        super.onResume()
        if (preferences.contains("pseudo"))
            tv_pseudo.text=preferences.getString("pseudo","")
        else
            tv_pseudo.text="Pas de pseudo"
        if (preferences.contains("pays"))
            tv_pays.text=preferences.getString("pays","")
        else
            tv_pays.text="Pas de pays"
        if (preferences.contains("age"))
            tv_age.text=preferences.getLong("age",0).toString()
        else
            tv_age.text="Pas d'âge"
    }
    override fun onClick(v: View) {
        val intent = Intent(this, EditionActivity::class.java)
        startActivity(intent)
    }

}