package iutlens.android.monprofil

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class EditionActivity : AppCompatActivity(), View.OnClickListener {
    private val fichierPreferences="prefsProfil"
    private lateinit var bt_enregistrer: Button
    private lateinit var bt_annuler: Button
    private lateinit var et_pseudo: EditText
    private lateinit var et_pays: EditText
    private lateinit var et_age: EditText
    private lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edition)
        et_pseudo=findViewById(R.id.id_et_pseudo)
        et_pays=findViewById(R.id.id_et_pays)
        et_age=findViewById(R.id.id_et_age)
        bt_enregistrer=findViewById(R.id.id_bt_enregistrer)
        bt_annuler=findViewById(R.id.id_bt_annuler)
        bt_annuler.setOnClickListener(this)
        bt_enregistrer.setOnClickListener(this)
        preferences = getSharedPreferences(fichierPreferences, Context.MODE_PRIVATE)
        if (preferences.contains("pseudo"))
            et_pseudo.setText(preferences.getString("pseudo",""))
        if (preferences.contains("pays"))
            et_pays.setText(preferences.getString("pays",""))
        if (preferences.contains("age"))
            et_age.setText(preferences.getLong("age",0).toString())
    }
    override fun onClick(v: View) {
        if (v.id==R.id.id_bt_annuler)
            finish()
        else if (v.id==R.id.id_bt_enregistrer){
            val editor=preferences.edit()
            if (et_pseudo.text.isNotEmpty())
                editor.putString("pseudo",et_pseudo.text.toString())
            if (et_pays.text.isNotEmpty())
                editor.putString("pays",et_pays.text.toString())
            if (et_age.text.isNotEmpty())
                editor.putLong("age",et_age.text.toString().toLong())
            editor.commit()
            finish()
        }
    }
}